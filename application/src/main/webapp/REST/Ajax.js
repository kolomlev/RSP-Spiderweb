"use strict";

import request from "superagent";
import Cookies from "js-cookie";

const csrfTokenHeader = "X-CSRF-Token";

class Ajax {
  constructor() {
    this.req = null;
  }

  get(url) {
    this.req = request.get(url, null, null).accept("json");
    return this;
  }

  post(url, data, type) {
    this.req = request
      .post(url)
      .type(type ? type : "json")
      .accept("json");
    if (data) {
      this.req = this.req.send(data);
    }
    return this;
  }

  attach(file) {
    // Remove the content type to force the browser to fill it in for us
    // See http://uncorkedstudios.com/blog/multipartformdata-file-upload-with-angularjs, section Making the
    // multipart/form-data request
    this.req = this.req.attach("file", file, file.name).type(null);
    return this;
  }

  put(url, data) {
    this.req = request.put(url).type("json");
    if (data) {
      this.req = this.req.send(data);
    }
    return this;
  }

  del(url) {
    this.req = request.del(url);
    return this;
  }

  send(data) {
    this.req = this.req.send(data);
    return this;
  }

  /**
   * Executes the previously configured request.
   * @param onSuccess Success handler, it is passed data parsed from the JSON in the response (if present) and the
   *     response itself
   * @param onError Error handler, called when the request returns a non-2xx status. If the error response contains a
   *     parseable JSON object, it is passed to the handler
   */
  end(onSuccess, onError) {
    this.req.set(csrfTokenHeader, this._getCsrfToken()).end(
      function(err, resp) {
        if (err) {
          console.log(err.status);
          if (err.status === 401 || err.status === 409) {
            // const currentRoute = Utils.getPathFromLocation();
            // if (currentRoute !== Routes.register.path && currentRoute !== Routes.login.path) {
            //     Routing.saveOriginalTarget({path: currentRoute});
            //     Routing.transitionTo(Routes.login);
            // }
            if (onError) {
              onError();
            }
            return;
          }
          try {
            if (onError) {
              onError(JSON.parse(err.response.text), err);
            }
            this._handleError(err);
          } catch (ex) {
            // The response text is not a parseable JSON
            this._handleError(err);
          }
        } else if (onSuccess) {
          onSuccess(resp.body, resp);
        }
      }.bind(this)
    );
  }

  _getCsrfToken() {
    const cookie = Cookies.get("CSRF-TOKEN");
    return cookie ? cookie : "";
  }

  _handleError(err) {
    try {
      const error = JSON.parse(err.response.text),
        method = err.response.req.method,
        msg =
          method +
          " " +
          error.requestUri +
          " - Status " +
          err.status +
          ": " +
          error.message;
      if (err.status === 404) {
        console.warn(msg);
      } else {
        console.error(msg);
      }
    } catch (ex) {
      console.error("AJAX error: " + err.response.text);
    }
  }
}

const INSTANCE = new Ajax();

export default INSTANCE;
