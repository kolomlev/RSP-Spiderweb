"use strict";

import Ajax from "./Ajax";
import Cookie from "./Cookie";

export default class Authentication {
  static login(username, password, errorCallback, okCallback) {
    Ajax.post("j_spring_security_check", null, "form")
      .send("username=" + username)
      .send("password=" + password)
      .end(
        function(err, resp) {
          if (err) {
            errorCallback();
            return;
          }
          const status = JSON.parse(resp.text);
          if (!status.success || !status.loggedIn) {
            errorCallback();
            return;
          }
          console.log("User successfully authenticated.");
          //přesměrování
          okCallback();
        }.bind(this)
      );
  }

  static logout(callback) {
    Ajax.post("j_spring_security_logout").end(function(err) {
      if (err) {
        console.error("Logout failed. Status: " + err.status);
      } else {
        console.log("User successfully logged out.");
        Cookie.deleteCookie("logged");
      }
      // Routing.transitionTo(Routes.login);
      // Cookie.deleteCookie("logged");
      window.localStorage.clear();
      callback();
    });
  }

  static authorize(loggedCallback, notLoggedcallback) {
    Ajax.get("rest/user/current").end(function(body, resp) {
      if (resp.status === 200) {
        console.log("logged in");
        Cookie.setCookie("logged", "true");
        window.localStorage.setItem("username", body.username);
        if (loggedCallback) {
          loggedCallback();
        }
      } else {
        console.log("not logged in");
        Cookie.deleteCookie("logged");
        window.localStorage.clear();
        notLoggedcallback();
      }
    });
  }
}
