/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export default class Privilage {
  constructor(match, organizer) {
    this.playerOne = match.playerOne;
    this.playerTwo = match.playerTwo;
    this.organizer = organizer;
    this.user = window.localStorage.getItem("username");
  }

  isPlayerOne() {
    return this.playerOne && this.playerOne.username === this.user;
  }

  isPlayerTwo() {
    return this.playerTwo && this.playerTwo.username === this.user;
  }

  isOrganizer() {
    return this.organizer.username === this.user;
  }

  equalsCurrentUser(user) {
    return user
      ? window.localStorage.getItem("username") === user.username ? true : false
      : false;
  }
}
