/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export default class StateStorage {
  constructor(key) {
    this.key = key;
  }

  set(value) {
    window.localStorage.setItem(this.key, JSON.stringify(value));
  }

  get() {
    return JSON.parse(window.localStorage.getItem(this.key));
  }

  remove() {
    window.localStorage.removeItem(this.key);
  }

  refreshById(key, id) {
    if (this.get(this.key) && this.get(this.key).id !== id) {
      window.localStorage.removeItem(key);
    }
  }
}
