import React from "react";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import {
  Table,
  Button,
  ButtonToolbar,
  Glyphicon,
  ButtonGroup,
  FormGroup,
  FormControl,
  ControlLabel,
  Col,
  Form,
  Panel,
  PageHeader
} from "react-bootstrap";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";
import Moment from "moment";
import MomentFormat from "moment-duration-format";
import Authentication from "../REST/Authentication.js";

export default class TournamentList extends React.Component {
  constructor() {
    super();
    this.state = {
      tournaments: [],
      filter: {}
    };
  }

  componentWillMount() {
    Ajax.get("rest/tournament").end(
      function(body, resp) {
        if (resp.status === 200) {
          this.flattenData(body);
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  }

  flattenData = data => {
    console.log(data);
    this.setState({
      tournaments: data.map(tournament => {
        return {
          id: tournament.id,
          name: tournament.name,
          sport: tournament.sport,
          organizer: tournament.organizer.username,
          place: tournament.place,
          date: Moment(tournament.date, "DD/MM/YYYY", false).format(),
          time: tournament.time,
          description: tournament.description,
          submitted: tournament.matches.length > 0 ? true : false
        };
      })
    });
  };

  dateFormatter = (date, row) => {
    return Moment(date).format("DD/MM/YYYY");
  };

  getExpandedRow(row) {
    return (
      <div>
        <div>
          <strong>Start</strong>: {row.time}
        </div>
        <div>
          <strong>Organizer</strong>: {row.organizer}
        </div>
        <div>
          <strong>Description</strong>: {row.description}
        </div>
      </div>
    );
  }

  getActions = (cell, row) => {
    return window.localStorage.getItem("username") === row.organizer ? (
      <ButtonToolbar>
        <ButtonGroup>
          <Button onClick={() => this.handleTournamentRedirect(row)}>
            <Glyphicon glyph="edit" />
          </Button>
          <Button onClick={() => this.handleTournamentRemove(row)}>
            <Glyphicon glyph="trash" />
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    ) : row.submitted ? (
      <ButtonToolbar>
        <ButtonGroup>
          <Button onClick={() => this.handleTournamentRedirect(row)}>
            <Glyphicon glyph="edit" />
          </Button>
        </ButtonGroup>
      </ButtonToolbar>
    ) : (
      <ButtonToolbar />
    );
  };

  handleTournamentRedirect = tournament => {
    Authentication.authorize(this.okCallback(tournament), () => {
      this.props.history.push("/");
    });
  };

  okCallback = tournament => {
    if (tournament.submitted) {
      this.props.history.push({
        pathname: "/tournament",
        state: { tournament: tournament }
      }); //Main Page
    } else {
      this.props.history.push({
        pathname: "/addplayers",
        state: { tournament: tournament }
      });
    }
  };

  handleTournamentRemove = tournament => {
    Ajax.del("rest/tournament/" + tournament.id).end(
      function(body, resp) {
        if (resp.status === 200) {
          var array = [...this.state.tournaments]; // make a separate copy of the array
          var index = array.indexOf(tournament);
          array.splice(index, 1);
          this.setState({ tournaments: array });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
    //TODO Ajax
  };

  handleFilterChange = event => {
    var filter = {
      participant: false,
      organizer: false
    };
    if (event.target.value !== "all") {
      filter[event.target.value] = true;
    }
    Ajax.post("rest/tournament/filter", filter).end(
      function(body, resp) {
        if (resp.status === 200) {
          this.flattenData(body);
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
    //TODO Ajax
  };

  render() {
    const options = {
      //expandRowBgColor: 'rgb(242, 255, 163)', TODO vyber nejakou hezci barvu
      expandBy: "column"
    };
    const filter = (
      <FormControl
        onChange={this.handleFilterChange}
        componentClass="select"
        placeholder="select"
      >
        <option value="all">All</option>
        <option value="organizer">Organizer</option>
        <option value="participant">Participant</option>
      </FormControl>
    );
    return (
      <div>
        <PageHeader>Tournament list</PageHeader>
        <Panel className="sw-content">
          <BootstrapTable
            options={options}
            keyField="id"
            data={this.state.tournaments}
            expandableRow={row => {
              return true;
            }}
            expandComponent={this.getExpandedRow}
            pagination
          >
            <TableHeaderColumn dataField="name" filter={{ type: "TextFilter" }}>
              Name
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="sport"
              filter={{ type: "TextFilter" }}
            >
              Sport
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="date"
              dataFormat={this.dateFormatter}
              filter={{
                type: "DateFilter",
                defaultValue: { date: new Date(), comparator: ">=" }
              }}
            >
              Date
            </TableHeaderColumn>
            <TableHeaderColumn
              dataField="place"
              filter={{ type: "TextFilter" }}
            >
              Place
            </TableHeaderColumn>
            <TableHeaderColumn
              width="150"
              dataField="button"
              expandable={false}
              dataFormat={this.getActions}
            >
              Role{filter}
            </TableHeaderColumn>
          </BootstrapTable>
        </Panel>
      </div>
    );
  }
}
