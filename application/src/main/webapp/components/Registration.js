import React from "react";
import {
  OverlayTrigger,
  Popover,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button,
  Alert
} from "react-bootstrap";
import FormControl from "react-bootstrap/lib/FormControl";
import Ajax from "../REST/Ajax";
import Authentication from "../REST/Authentication";
import Cookie from "../REST/Cookie";

export default class Registration extends React.Component {
  constructor(props) {
    super(props);
    this.errCallback = this.props.errCallback;
    this.state = {
      username: "",
      password: "",
      name: "",
      surname: "",
      confirmation: "",
      usernameStatus: null,
      passwordStatus: null,
      validate: false
    };
  }

  /*Registration function. Sends registration data to backend*/
  registrate = () => {
    const user = {
      username: this.state.username, //name like in Java
      password: this.state.password
    };

    //TODO map to Java
    Ajax.post("rest/registration", user).end(
      function(body, resp) {
        if (resp.status === 201) {
          Authentication.login(
            user.username,
            user.password,
            this.errorCallback,
            this.callback
          );
        }
      }.bind(this),
      function() {
        this.errCallback("Username already exists");
      }.bind(this)
    );
  };
  callback = () => {
    console.log("Login ok. Setting cookie and redirect");
    Cookie.setCookie("logged", "true");
    window.localStorage.setItem("username", this.state.username);
    this.props.history.push("/");
  };
  errorCallback = () => {
    console.log("Error"); //TODOerror callback
  };

  /*Validation. If everything is OK, calls registration, otherwise render error alert/popover*/
  validate = () => {
    let ok = true;
    this.setState({ validate: true });
    if (this.state.username.length < 6) {
      this.setState({ usernameStatus: "error" });
      ok = false;
    }
    if (this.state.password.length < 8) {
      this.setState({ passwordStatus: "error" });
      ok = false;
    } else {
      this.setState({ passwordStatus: "success" });
    }
    if (this.state.password != this.state.confirmation) {
      this.setState({ passwordStatus: "error" });
      this.setState({ password: "" });
      this.setState({ confirmation: "" });
      ok = false;
    }
    if (ok) {
      this.registrate();
    }
  };

  /*Handles changes in username, password and confirmation inputs*/
  handleChangeUsername = e => {
    this.setState({ username: e.target.value });
    if (this.state.username.length > 4) {
      //it starts from 0
      this.setState({ usernameStatus: "success" });
    } else {
      this.setState({ usernameStatus: null });
    }
  };
  handleChangePassword = e => {
    this.setState({ password: e.target.value });
    this.setState({ passwordStatus: null });
  };
  hadndleChangeConfirm = e => {
    this.setState({ confirmation: e.target.value });
    this.setState({ passwordStatus: null });
  };

  handleInfoChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  /*Utils to show error and tooltips*/
  getValidationState = type => {
    if (type === "username") {
      return this.state.usernameStatus;
    } else {
      return this.state.passwordStatus;
    }
  };

  getInfoState = field => {
    if (!this.state.validate) {
      return null;
    }
    if (this.state[key]) {
      return "success";
    }
    return "error";
  };

  getPopover = type => {
    switch (type) {
      case "username-default":
        return (
          <Popover id="popover-trigger-hover" title="Username">
            Username has to contain min 6 letters/numbers
          </Popover>
        );
    }

    //TODO ADD ERROR POPOVERS (or alerts)
  };

  render() {
    return (
      <Form horizontal={true} className="form">
        <FormGroup controlId="name" validationState={this.getInfoState("name")}>
          <Col componentClass={ControlLabel} htmlFor="name" sm={3}>
            Name
          </Col>
          <Col sm={12} md={6}>
            <FormControl
              type="text"
              placeholder="Name"
              value={this.state.name}
              onChange={this.handleInfoChange}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="surname"
          validationState={this.getInfoState("surname")}
        >
          <Col componentClass={ControlLabel} htmlFor="surname" sm={3}>
            Surname
          </Col>
          <Col sm={12} md={6}>
            <FormControl
              type="text"
              placeholder="Surname"
              value={this.state.surname}
              onChange={this.handleInfoChange}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <OverlayTrigger
          trigger="focus"
          placement="bottom"
          overlay={this.getPopover("username-default")}
        >
          <FormGroup
            controlId="username"
            validationState={this.getValidationState("username")}
          >
            <Col componentClass={ControlLabel} htmlFor="username" sm={3}>
              Username
            </Col>
            <Col sm={12} md={6}>
              <FormControl
                type="text"
                placeholder="Username"
                value={this.state.username}
                onChange={this.handleChangeUsername}
              />
              <FormControl.Feedback />
            </Col>
          </FormGroup>
        </OverlayTrigger>
        <FormGroup
          controlId="password"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="password" sm={3}>
            Password
          </Col>
          <Col sm={12} md={6}>
            <FormControl
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.handleChangePassword}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="confirm"
          validationState={this.getValidationState("password")}
        >
          <Col componentClass={ControlLabel} htmlFor="confirm" sm={3}>
            Confirm Your Password
          </Col>
          <Col sm={12} md={6}>
            <FormControl
              type="password"
              placeholder="Confirm Your Password"
              value={this.state.confirmation}
              onChange={this.hadndleChangeConfirm}
            />
            <FormControl.Feedback />
          </Col>
        </FormGroup>
        <FormGroup>
          <Col mdOffset={3} md={6} sm={10}>
            <Button
              type="submit"
              block
              bsStyle="success"
              onClick={this.validate}
            >
              Sign up
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
  }
}
