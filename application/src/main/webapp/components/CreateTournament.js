import React from "react";
import {
  OverlayTrigger,
  Popover,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button,
  FormControl,
  Panel,
  PageHeader
} from "react-bootstrap";
import DatePicker from "react-bootstrap-date-picker";
import TimePicker from "react-bootstrap-time-picker";
import Ajax from "../REST/Ajax";
import Moment from "moment";
import MomentFormat from "moment-duration-format";
import StateStorage from "../REST/StateStorage";
import Authentication from "../REST/Authentication.js";

export default class CreateTournament extends React.Component {
  constructor() {
    super();
    this.stateStoreage = new StateStorage("create");
    if (this.stateStoreage.get()) {
      this.state = this.stateStoreage.get();
    } else {
      const today = new Date().toISOString();
      this.state = {
        name: "",
        sport: "",
        place: "",
        description: "",
        date: Moment(new Date()).format("DD/MM/YYYY"),
        dateValue: today,
        minDate: today,
        time: "07:00",
        validate: false
      };
    }
  }

  componentDidMount() {
    this.stateStoreage.set(this.state);
  }

  componentDidUpdate() {
    this.stateStoreage.set(this.state);
  }

  handleChangeDate = (dateValue, formattedValue) => {
    this.setState({
      dateValue: dateValue,
      date: formattedValue // Formatted String, ex: "11/19/2016"
    });
  };

  handleChangeTime = seconds => {
    var time = Moment.duration(seconds, "seconds").format("hh:mm", {
      trim: false
    });
    this.setState({ time: time });
  };

  handleChangeInput = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  handleSubmit = e => {
    e.preventDefault();
    Authentication.authorize(this.okCallback, () => {
      this.props.history.push("/");
    });
  };

  okCallback = () => {
    this.setState({ validate: true });
    const tournament = {
      name: this.state.name,
      sport: this.state.sport,
      place: this.state.place,
      description: this.state.description,
      date: this.state.date,
      time: this.state.time,
      matches: []
    };

    if (this.validate(tournament)) {
      Ajax.post("rest/tournament", tournament).end(
        function(body, resp) {
          if (resp.status === 201) {
            //TODO redirect to add players page
            this.stateStoreage.remove();
            this.props.history.push({
              pathname: "/addplayers",
              state: { tournament: body }
            }); //Main Page
          }
        }.bind(this),
        function(err) {
          alert("SMTH is wrong"); //TODO customize error
        }.bind(this)
      );
    }
  };

  getValidationState = key => {
    if (!this.state.validate) {
      return null;
    }
    if (this.state[key]) {
      return "success";
    }
    return "error";
  };

  validate = tournament => {
    if (
      tournament.name &&
      tournament.sport &&
      tournament.place &&
      tournament.description &&
      tournament.date &&
      tournament.time
    ) {
      return true;
    }
    return false;
  };

  render() {
    return (<div>
    <PageHeader>Create tournament</PageHeader>
      <Panel className="sw-content">
        <Form onSubmit={this.handleSubmit} className="sw-create" horizontal={true}>
          <FormGroup
            validationState={this.getValidationState("name")}
            controlId="name"
          >
            <Col componentClass={ControlLabel} htmlFor="name">
              Name
            </Col>
            <Col >
              <FormControl
                type="text"
                placeholder="Name"
                value={this.state.name}
                onChange={this.handleChangeInput}
              />
            </Col>
          </FormGroup>
          <FormGroup
            validationState={this.getValidationState("sport")}
            controlId="sport"
          >
            <Col componentClass={ControlLabel} htmlFor="sport">
              Sport
            </Col>
            <Col>
              <FormControl
                type="text"
                placeholder="Sport"
                value={this.state.sport}
                onChange={this.handleChangeInput}
              />
            </Col>
          </FormGroup>
          <FormGroup
            validationState={this.getValidationState("place")}
            controlId="place"
          >
            <Col componentClass={ControlLabel} htmlFor="place">
              Place
            </Col>
            <Col>
              <FormControl
                type="text"
                placeholder="Place"
                value={this.state.place}
                onChange={this.handleChangeInput}
              />
            </Col>
          </FormGroup>
          <FormGroup
            validationState={this.getValidationState("description")}
            controlId="description"
          >
            <Col componentClass={ControlLabel} htmlFor="description">
              Description
            </Col>
            <Col >
              <FormControl
                type="text"
                placeholder="Description"
                value={this.state.description}
                onChange={this.handleChangeInput}
              />
            </Col>
          </FormGroup>
          <FormGroup
            validationState={this.getValidationState("date")}
            controlId="date"
          >
            <Col componentClass={ControlLabel} htmlFor="date">
              Date
            </Col>
            <Col >
              <DatePicker
                minDate={this.state.minDate}
                showClearButton={false}
                value={this.state.dateValue}
                onChange={this.handleChangeDate}
              />
            </Col>
          </FormGroup>
          <FormGroup
            validationState={this.getValidationState("time")}
            controlId="time"
          >
            <Col componentClass={ControlLabel} htmlFor="time">
              Time
            </Col>
            <Col>
              <TimePicker
                value={this.state.time}
                format={24}
                onChange={this.handleChangeTime}
                start="00:00"
                end="23:30"
                step={30}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col>
              <Button className="pull-right" type="submit">Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      </Panel>
      </div>
    );
  }
}
