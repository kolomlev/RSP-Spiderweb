import React from "react";
import Match from "./Match.js";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import { PageHeader } from "react-bootstrap";
export default class Tournament extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      id: "",
      name: "",
      organizer: "",
      matches: [],
      render: false
    };
  }

  componentDidMount() {
    this.stateStorage.set(this.state.id);
  }

  componentDidUpdate() {
    this.stateStorage.set(this.state.id);
  }

  componentWillUnmount() {
    this.stateStorage.remove();
  }
  componentWillMount() {
    this.stateStorage = new StateStorage("tournament");
    const tid = this.props.location.state
      ? this.props.location.state.tournament.id
      : this.stateStorage.get();
    Ajax.get("rest/tournament/" + tid).end(
      function(body, resp) {
        if (resp.status === 200) {
          console.log("ok");
          const id = body.id;
          const organizer = body.organizer;
          const matches = body.matches.map((match, index) => {
            return (
              <Match
                key={index}
                index={index}
                update={this.updateCallback}
                organizer={organizer}
                data={match}
              />
            );
          });
          const name = body.name;
          this.setState({
            id,
            name,
            organizer,
            matches
          });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  }

  updateCallback = () => {
    //TODO rerender match components only
    window.location.reload();
  };

  render() {
    console.log(this.state.matches);
    return (
      <div>
        <PageHeader>{this.state.name}</PageHeader>
        {this.getTreeJSX(this.state.matches)}
      </div>
    );
  }
  getTreeJSX = data => {
    return <div className="main">{this.getRounds(data)}</div>;
  };
  getRounds = data => {
    let toR = [];
    for (var i = this.getBaseLog(2, data.length + 1) - 1; i >= 0; i--) {
      toR.push(
        <div key={i + 1} className={"col col_" + (i + 1)}>
          {this.getMatches(i, data)}
        </div>
      );
    }
    return toR;
  };
  getMatches = (i, data) => {
    let toR = [];
    for (var j = Math.pow(2, i + 1) - 1; j > Math.pow(2, i) - 1; j--) {
      toR.push(
        <div key={j - 1} className={"data data_" + (i + 1)}>
          {data[j - 1]}
        </div>
      );
    }
    return toR;
  };

  getBaseLog = (x, y) => {
    return Math.log(y) / Math.log(x);
  };
}
