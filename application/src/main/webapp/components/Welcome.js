import React from "react";
import {
  Button,
  Grid,
  Row,
  Col,
  Tab,
  Nav,
  NavItem,
  Transition,
  Alert
} from "react-bootstrap";
import Login from "./Login";
import Registration from "./Registration";

export default class Welcome extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: false,
      message: ""
    };
  }
  clickLogin = () => {
    this.props.history.push("/login");
  };
  clickRegistrate = () => {
    this.props.history.push("/registrate");
  };

  showAlert = message => {
    this.setState({ error: true, message });
  };

  /*Closes alert*/
  handleDismiss = () => {
    this.setState({ error: false });
  };
  getAlert = () => {
    return (alert = (
      <Alert bsStyle="danger" onDismiss={this.handleDismiss}>
        <h4>Oh snap! You got an error!</h4>
        <p>{this.state.message}</p>
      </Alert>
    ));
  };

  render() {
    const welcome = (
      <div className="welcome">
        <h1>Welcome</h1>
        <br />

        <p>
          Creating brackets online - have never been easier. SpiderWeb allows
          you to easily create and manage multiple tournaments online. Add
          participants, manage games, announce winners, solve conflicts and let
          players write down their scores.
        </p>
        <img src="spiderweb-logo.png" className="welcome-logo" />
      </div>
    );
    const login = (
      <Login errCallback={this.showAlert} history={this.props.history} />
    );
    const registration = (
      <Registration errCallback={this.showAlert} history={this.props.history} />
    );
    const content = (
      <Grid>
        <Row>{this.state.error ? this.getAlert() : ""}</Row>
        <Row>
          <Col md={6} sm={6} xs={6}>
            <Tab.Container id="sign" defaultActiveKey="first">
              <div>
                <Row className="clearfix tab-content">
                  <Col sm={12} md={12}>
                    <Tab.Content animation>
                      <Tab.Pane eventKey="first">
                        <h1>Sign in</h1>
                        {login}
                      </Tab.Pane>
                      <Tab.Pane eventKey="second">
                        <h1>Sign up</h1>
                        {registration}
                      </Tab.Pane>
                    </Tab.Content>
                  </Col>
                </Row>
                <Row className="rowPills">
                  <Col sm={12} md={12}>
                    <Nav justified bsStyle="pills">
                      <NavItem eventKey="first">
                        Already have an account? Sign in
                      </NavItem>
                      <NavItem eventKey="second">Create new account</NavItem>
                    </Nav>
                  </Col>
                </Row>
              </div>
            </Tab.Container>
          </Col>
          <Col md={6} sm={6} xs={4}>
            {welcome}
          </Col>
        </Row>
      </Grid>
    );
    return <div className="welcome-content">{content}</div>;
  }
}
