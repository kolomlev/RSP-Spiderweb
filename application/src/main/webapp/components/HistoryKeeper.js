import React from "react";
import Cookie from "../REST/Cookie";
import Ajax from "../REST/Ajax";
import Authentication from "../REST/Authentication.js";

export default class Keeper extends React.Component {
  constructor(props) {
    super(props);
    let path;
    window.onbeforeunload = () => {
      console.log("onbeforeunload");
      console.log(this.props.location.pathname);
      localStorage.setItem("path", this.props.location.pathname);
    };
  }
  componentWillMount() {
    console.log("keeper");
    if (localStorage.getItem("path")) {
      this.refresh();
    }
    Authentication.authorize(this.loggedCallback, this.notLoggedCallback);
  }

  loggedCallback = () => {
    console.log("logged in");
    Cookie.setCookie("logged", "true");
  };

  notLoggedCallback = () => {
    Cookie.deleteCookie("logged");
    window.localStorage.clear();
    this.props.history.push("/");
  };

  refresh() {
    console.log("onload");
    console.log(localStorage.getItem("path"));
    this.props.history.push(localStorage.getItem("path"));
  }

  render() {
    return <div />;
  }
}
