import React from "react";
import {
  Panel,
  Form,
  FormGroup,
  FormControl,
  ControlLabel,
  Col,
  Modal,
  Button
} from "react-bootstrap";
import Privilage from "../REST/Privilage";
import Result from "../REST/Result";
import Ajax from "../REST/Ajax";
import Authentication from "../REST/Authentication.js";
import { BootstrapTable, TableHeaderColumn } from "react-bootstrap-table";

//Reprezentuje zápas
export default class Match extends React.Component {
  constructor(props) {
    super(props);
    this.privilage = new Privilage(this.props.data, this.props.organizer);
    this.state = {
      id: this.props.data.id,
      playerOne: this.props.data.playerOne,
      playerTwo: this.props.data.playerTwo,
      organizer: this.props.organizer,
      scoreOne: this.getScore("scoreOne"),
      scoreTwo: this.getScore("scoreTwo"),
      update: this.props.update,
      status: this.props.data.status,
      notes: this.props.notes ? this.props.notes.split(",") : [],
      newNote: "",
      show: false,
      validate: false
    };
  }

  hasPermission = () => {
    return (
      this.privilage.isPlayerOne() ||
      this.privilage.isPlayerTwo() ||
      this.privilage.isOrganizer()
    );
  };

  getStatusColor = () => {
    var backgroundColor = "#f8f8f8";
    if (this.state.status === "TBD") {
      backgroundColor = "#faebcc";
    } else if (this.state.status === "CONFLICT") {
      backgroundColor = "#ebccd1";
    } else if (this.state.status === "OK") {
      backgroundColor = "#d6e9c6";
    }
    return { backgroundColor };
  };

  hasBothPlayers = () => {
    return this.props.data.playerOne && this.props.data.playerTwo;
  };

  handleBlur = () => {
    //TODO AJax submit
    Authentication.authorize(this.okCallback, () => {
      this.props.history.push("/");
    });
  };

  okCallback = () => {
    this.setState({ validate: true });
    if (this.validate()) {
      const result = {
        scoreOne: this.state.scoreOne,
        scoreTwo: this.state.scoreTwo
      };
      Ajax.post("rest/match/" + this.state.id + "/setResult", result).end(
        function(body, resp) {
          if (resp.status === 200) {
            this.state.status = body.status;
            this.state.update();
          }
        }.bind(this),
        function(err) {
          alert("SMTH is wrong"); //TODO customize error
        }.bind(this)
      );
    }
  };

  validate = () => {
    return this.state.scoreOne && this.state.scoreTwo;
  };

  getValidationState = key => {
    if (!this.state.validate) {
      return null;
    }
    if (this.state[key]) {
      return "success";
    }
    return "error";
  };

  getResults = () => {
    var results = [this.props.data.resultOne, this.props.data.resultTwo];
    const flat = results.map(r => {
      console.log(r);
      return {
        scoreOne: r.scoreOne,
        scoreTwo: r.scoreTwo,
        author: r.author.username,
        notes: r.notes
      };
    });
    return flat;
  };

  //TODO this shit is disgusting and needs rework
  getScore = key => {
    if (this.props.data.status === "OK" && this.hasBothPlayers()) {
      return this.props.data.resultOne[key];
    }
    if (this.props.data.status === "CONFLICT" && this.privilage.isOrganizer()) {
      return;
    }
    if (this.privilage.isPlayerOne() && this.props.data.resultOne) {
      return this.props.data.resultOne[key];
    }
    if (this.privilage.isPlayerTwo() && this.props.data.resultTwo) {
      return this.props.data.resultTwo[key];
    }
  };

  handleScoreChange = e => {
    this.setState({ [e.target.id]: e.target.value });
  };

  getExpandedRow(row) {
    return (
      <div>
        <div>
          <strong>Notes</strong>: {row.notes}
        </div>
      </div>
    );
  }

  render() {
    return (
      <Panel
        onBlur={() => this.handleBlur()}
        style={this.getStatusColor()}
        className="sw-match"
      >
        <Form horizontal>
          <FormGroup
            controlId="scoreOne"
            validationState={this.getValidationState("scoreOne")}
          >
            <Col componentClass={ControlLabel} lg={3}>
              {this.state.playerOne ? this.state.playerOne.username : "Pending"}
            </Col>
            <Col className="sw-match-input" lg={2}>
              <FormControl
                disabled={
                  !this.hasPermission() ||
                  !this.hasBothPlayers() ||
                  (this.state.status === "OK" && !this.privilage.isOrganizer())
                }
                type="text"
                placeholder={
                  this.state.status === "CONFLICT" ? "Conflict" : "Score"
                }
                onChange={this.handleScoreChange}
                defaultValue={this.getScore("scoreOne")}
              />
            </Col>
          </FormGroup>
          <FormGroup
            controlId="scoreTwo"
            validationState={this.getValidationState("scoreTwo")}
          >
            <Col componentClass={ControlLabel} lg={3}>
              {this.state.playerTwo ? this.state.playerTwo.username : "Pending"}
            </Col>
            <Col className="sw-match-input" lg={2}>
              <FormControl
                disabled={
                  !this.hasPermission() ||
                  !this.hasBothPlayers() ||
                  (this.state.status === "OK" && !this.privilage.isOrganizer())
                }
                type="text"
                placeholder={
                  this.state.status === "CONFLICT" ? "Conflict" : "Score"
                }
                onChange={this.handleScoreChange}
                defaultValue={this.getScore("scoreTwo")}
              />
            </Col>
          </FormGroup>
          <FormGroup>
            <Col lgOffset={8} lg={2}>
              <a onClick={this.handleShow}>Details</a>
            </Col>
          </FormGroup>
        </Form>
        <Modal show={this.state.show} onHide={this.handleClose}>
          <Modal.Header closeButton>
            <Modal.Title>Details</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            {this.state.status === "CONFLICT" &&
            this.privilage.isOrganizer() ? (
              <div>
                <h4>Conflict</h4>
                <BootstrapTable
                  data={this.getResults()}
                  expandableRow={row => {
                    return true;
                  }}
                  expandComponent={this.getExpandedRow}
                >
                  <TableHeaderColumn dataField="author" isKey={true}>
                    Author
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="scoreOne">
                    Score: {this.state.playerOne.username}
                  </TableHeaderColumn>
                  <TableHeaderColumn dataField="scoreTwo">
                    Score: {this.state.playerTwo.username}
                  </TableHeaderColumn>
                </BootstrapTable>
              </div>
            ) : (
              ""
            )}

            {this.state.notes.map((item, index) => (
              <div key={index}>
                {item}
                <hr />
              </div>
            ))}
            <Form>
              <FormGroup controlId="Notes">
                <ControlLabel>
                  <h4>Notes</h4>
                </ControlLabel>
                <FormControl
                  componentClass="textarea"
                  onChange={this.handleNotes}
                  value={this.state.newNote}
                  placeholder="Add notes"
                  disabled={!this.hasPermission()}
                />
                {this.hasPermission() ? (
                  <Button onClick={this.submitNotes}>Add notes</Button>
                ) : (
                  ""
                )}
              </FormGroup>
            </Form>
          </Modal.Body>
        </Modal>
      </Panel>
    );
  }
  handleClose = () => {
    this.setState({ show: false });
  };

  handleShow = () => {
    this.setState({ show: true });
  };
  submitNotes = e => {
    console.log(this.state.notes);
    let n = this.state.notes;
    n.push(this.state.newNote);
    this.setState({ notes: n });
    this.setState({ newNote: "" });
    /*Ajax send this.state.notes.toString()*/
    console.log(this.state.notes.toString());
  };

  handleNotes = e => {
    this.setState({ newNote: e.target.value });
  };
}
