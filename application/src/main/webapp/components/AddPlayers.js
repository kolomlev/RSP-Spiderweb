import React from "react";
import {
  ListGroup,
  ListGroupItem,
  Button,
  Glyphicon,
  Form,
  FormGroup,
  Col,
  MenuItem,
  Panel,
  PageHeader
} from "react-bootstrap";
import { AsyncTypeahead } from "react-bootstrap-typeahead";
import Ajax from "../REST/Ajax";
import StateStorage from "../REST/StateStorage";
import Authentication from "../REST/Authentication.js";

export default class AddPlayers extends React.Component {
  constructor(props) {
    super(props);
    this.stateStorage = new StateStorage("add");
    if (!this.props.location.state) {
      this.state = this.stateStorage.get();
    } else {
      this.state = {
        allowNew: false,
        isLoading: false,
        options: [],
        participants: [],
        tournament: this.props.location.state.tournament
      };
    }
  }

  componentDidMount() {
    this.stateStorage.set(this.state);
  }

  componentDidUpdate() {
    this.stateStorage.set(this.state);
  }

  componentWillUnmount() {
    this.stateStorage.remove();
  }

  handleSubmit = e => {
    e.preventDefault();
    Authentication.authorize(this.okCallback, () => {
      this.props.history.push("/");
    });
  };

  okCallback = () => {
    const data = {
      id: this.state.tournament.id,
      playersUsernames: this.state.participants.map(participant => {
        return participant.username;
      })
    };
    Ajax.post("rest/tournament/submit", data).end(
      function(body, resp) {
        if (resp.status === 200) {
          this.stateStorage.remove();
          this.props.history.push({
            pathname: "/tournament",
            state: { tournament: body }
          });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  };

  handleSearch = query => {
    this.setState({ isLoading: true });
    Ajax.get("rest/user?search=" + query).end(
      function(newUsers, resp) {
        if (resp.status === 200) {
          var options = this.state.options;
          newUsers.map(user => {
            if (!this.findParticipantInArray(this.state.options, user)) {
              options = [...options, user];
            }
          });
          this.setState({
            isLoading: false,
            options
          });
        }
      }.bind(this),
      function(err) {
        alert("SMTH is wrong"); //TODO customize error
      }.bind(this)
    );
  };

  handleSelect = selected => {
    this.typeahead.getInstance().clear();
    if (!this.findParticipantInArray(this.state.participants, selected)) {
      this.setState({
        participants: [...this.state.participants, selected]
      });
    }
  };

  findParticipantInArray = (array, user) => {
    for (var i = 0; i < array.length; i++) {
      if (array[i].username == user.username) {
        return array[i];
      }
    }
    return false;
  };

  handleRemove = participant => {
    var array = [...this.state.participants]; // make a separate copy of the array
    var index = array.indexOf(participant);
    array.splice(index, 1);
    this.setState({ participants: array });
  };

  renderParticipantList = () => {
    if (this.state.participants.length != 0) {
      return (
        <ListGroup>
          {this.state.participants.map((participant, index) => {
            return (
              <ListGroupItem key={index}>
                {participant.username}
                <Button
                  onClick={() => this.handleRemove(participant)}
                  bsSize="xsmall"
                  className="pull-right"
                >
                  <Glyphicon glyph="trash" />
                </Button>
              </ListGroupItem>
            );
          })}
        </ListGroup>
      );
    }
  };

  render() {
    return (<div>
       <PageHeader> Add players to <strong>{this.state.tournament.name}</strong></PageHeader>    
      <Panel className="sw-content">
       
        <Form onSubmit={this.handleSubmit} className="sw-create" horizontal={true}>
        <FormGroup controlId="search">
            <Col><AsyncTypeahead
            {...this.state}
            ref={typeahead => (this.typeahead = typeahead)}
            multiple={true}
            labelKey="username" 
            minLength={3}
            onSearch={this.handleSearch}
            placeholder="Search for a user..."
            onChange={selected => {
              this.handleSelect(selected[0]);
            }}
            renderMenuItemChildren={(option, props) => (
              <div>
                <strong>{option.username}</strong>
              </div>
            )}
          /></Col>
          </FormGroup>
          
          <FormGroup controlId="participants">
            <Col>{this.renderParticipantList()}</Col>
          </FormGroup>
          <FormGroup>
            <Col>
              <Button className="pull-right" type="submit">Submit</Button>
            </Col>
          </FormGroup>
        </Form>
      </Panel>
      </div>
    );
  }
}
