import React from "react";
import Link from "react-router-dom";
import Cookie from "../REST/Cookie";
import { Navbar, NavItem, Nav, NavDropdown, MenuItem } from "react-bootstrap";
import Authentication from "../REST/Authentication";

//společný pro všechny komponenty, krome Welcome
export default class NavBar extends React.Component {
  constructor(props) {
    super(props);
  }
  clickHandlerT = () => {
    Authentication.authorize(this.callbackT, this.callback);
  };

  clickHandlerC = () => {
    Authentication.authorize(this.callbackC, this.callback);
  };

  callbackT = () => {
    console.log("Callback T");
    this.props.history.push("/tournamentlist");
  };

  callbackC = () => {
    console.log("Callback C");
    this.props.history.push("/create");
  };

  logout = () => {
    Authentication.logout(this.callback);
  };
  callback = () => {
    console.log("Callback reload");
    this.props.history.push("/");
    //window.location.reload();
  };
  render() {
    //TODO budeme to delat pres cookie?
    // if(Cookie.getCookie("logged")=="true"){
    return (
      <Navbar fixedTop className="sw-nav" collapseOnSelect>
        <Navbar.Header className="sw-navheader">
          <Navbar.Brand>
            <a href="#" onClick={this.clickHandlerT}>
              <img src="./spiderweb-logo.png" />
            </a>
          </Navbar.Brand>
          <Navbar.Toggle />
        </Navbar.Header>
        <Navbar.Collapse>
          <Nav>
            <NavItem eventKey={1} onClick={this.clickHandlerT}>
              Tournaments
            </NavItem>
            <NavItem eventKey={2} onClick={this.clickHandlerC}>
              Create
            </NavItem>
          </Nav>
          <Nav pullRight>
            <NavItem eventKey={1} onClick={this.logout}>
              Logout
            </NavItem>
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    );
    ///return normal navBar
    // }else{
    //   //return nothing
    //   return <div>Nothing</div>;
    // }
  }
}
