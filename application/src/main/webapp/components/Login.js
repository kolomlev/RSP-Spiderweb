import React from "react";
import Authentication from "../REST/Authentication.js";
import {
  Alert,
  Col,
  Form,
  FormGroup,
  ControlLabel,
  Button
} from "react-bootstrap";
import FormControl from "react-bootstrap/lib/FormControl";
import Cookie from "../REST/Cookie";

export default class Login extends React.Component {
  constructor(props) {
    super(props);
    this.errCallback = this.props.errCallback;
    this.state = {
      username: "",
      password: "",
      empty: "false"
    };
  }
  /*Handlers of Username and password inputs*/
  onChangeUsername = e => {
    this.setState({ username: e.target.value });
    this.setState({ empty: "false" });
  };
  onChangePassword = e => {
    this.setState({ password: e.target.value });
    this.setState({ empty: "false" });
  };

  /*Login function, sends data to backend*/
  login = e => {
    e.preventDefault();
    if (this.state.username != "" && this.state.password != "") {
      Authentication.login(
        this.state.username,
        this.state.password,
        this.errorCallback,
        this.callback
      );
    } else {
      this.setState({ empty: "true" });
    }
  };
  callback = () => {
    console.log("Login ok. Setting cookie and redirect");
    Cookie.setCookie("logged", "true");
    window.localStorage.setItem("username", this.state.username);
    this.props.history.push("/");
  };
  /*Function is callback from login()*/
  errorCallback = () => {
    this.errCallback("Wrong username or password");
  };

  /*Util to show error*/
  getValidationState = () => {
    if (this.state.empty == "false") {
      return null;
    } else {
      return "error";
    }
  };

  render() {
    const form = (
      <Form className="form" horizontal>
        <FormGroup
          controlId="username"
          validationState={this.getValidationState()}
        >
          <Col componentClass={ControlLabel} htmlFor="username" sm={3}>
            Username
          </Col>
          <Col md={6} sm={12}>
            <FormControl
              type="text"
              placeholder="Username"
              value={this.state.username}
              onChange={this.onChangeUsername}
            />
          </Col>
        </FormGroup>
        <FormGroup
          controlId="password"
          validationState={this.getValidationState()}
        >
          <Col componentClass={ControlLabel} htmlFor="password" sm={3}>
            Password
          </Col>
          <Col md={6} sm={12}>
            <FormControl
              type="password"
              placeholder="Password"
              value={this.state.password}
              onChange={this.onChangePassword}
            />
          </Col>
        </FormGroup>

        <FormGroup>
          <Col mdOffset={3} md={6} sm={12}>
            <Button type="submit" block bsStyle="success" onClick={this.login}>
              Sign in
            </Button>
          </Col>
        </FormGroup>
      </Form>
    );
    return form;
  }
}
