package cz.cvut.fel.rsp.dto;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;

import java.util.Date;
import java.util.List;


@Getter
@Setter
public class TournamentDTO {

    private int id;

    private List<String> playersUsernames;

}
