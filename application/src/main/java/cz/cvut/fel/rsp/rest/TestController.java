package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.dto.FilterDto;
import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.service.repository.MatchService;
import cz.cvut.fel.rsp.service.repository.PersonService;
import cz.cvut.fel.rsp.service.repository.ResultService;
import cz.cvut.fel.rsp.service.repository.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/test")
public class TestController extends AbstractController {

    private final PersonService userService;

    private final ResultService resultService;

    private final MatchService matchService;

    private final TournamentService tournamentService;

    @Autowired
    public TestController(PersonService userService, ResultService resultService, MatchService matchService, TournamentService tournamentService) {
        this.userService = userService;
        this.resultService = resultService;
        this.matchService = matchService;
        this.tournamentService = tournamentService;
    }

    @RequestMapping(method = RequestMethod.GET)
    @Transactional
    public String test(){
        //Create user
        User user = new User();
        user.setUsername("123");
        user.setPassword("123");
        userService.persist(user);

        Tournament tournament = new Tournament();
        tournament.setName("Ololo");
        tournament.setOrganizer(user);
        tournamentService.persist(tournament);

        tournamentService.addPlayer(tournament.getId(),user.getUsername());

        Match m = new Match();
        m.setTournament(tournament);
        matchService.persist(m);

        Result r = new Result();
        r.setMatch(m);
        r.setScoreOne(10);
        r.setScoreTwo(2);
        r.setAuthor(user);
        resultService.persist(r);



        return "Hello";
    }

    //Sometimes it's easier, than write a test
    @RequestMapping(method = RequestMethod.GET, value = "/filter-{id}")
    public List<Tournament> filterTest(@PathVariable("id") Integer id){
        FilterDto filterDto = new FilterDto();
        //filterDto.setParticipantId(id);

        return tournamentService.findTournamentsByFilter(filterDto);
    }
}
