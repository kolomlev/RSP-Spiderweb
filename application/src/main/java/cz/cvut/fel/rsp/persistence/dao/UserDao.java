package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import java.util.List;
import java.util.Optional;

@Repository
public class UserDao extends BaseDao<User> {

    public UserDao() {
        super(User.class);
    }

    /**
     * Finds a {@link User} instance with the specified username.
     *
     * @param username Username to filter by
     * @return Matching instance, {@code null} if none matches
     */
    public User findByUsername(String username) {
        if (username == null || username.isEmpty())
            return null;
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<User> criteriaQuery = criteriaBuilder.createQuery(User.class);
        Root<User> from = criteriaQuery.from(User.class);
        criteriaQuery.select(from);
        // case-insesitive search
        criteriaQuery.where(criteriaBuilder.equal(criteriaBuilder.upper(from.<String>get("username")),username.toUpperCase()));
        List<User> fetchedUsers = em.createQuery(criteriaQuery).getResultList();
        if (fetchedUsers.size() == 0)
            return null;
        else
            return fetchedUsers.get(0);
    }

    /**
     * Checks whether an instance with the specified username exists.
     *
     * @param username The username to search for
     * @return Record existence status
     */
    public boolean exists(String username) {
        final List result = em.createNativeQuery("SELECT 1 FROM person WHERE username=?")
                              .setParameter(1, username).getResultList();
        return !result.isEmpty();
    }

    public List<User> findUserByString(String searchString) {
        return em.createNamedQuery("User.findByString", User.class)
                .setParameter("username", searchString + "%")
                .getResultList();
    }
}
