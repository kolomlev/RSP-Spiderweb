package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.dao.UserDao;
import cz.cvut.fel.rsp.service.repository.PersonService;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.awt.*;
import java.util.List;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

@RestController
@RequestMapping("/user")
public class UserController extends AbstractController {

    private final SecurityUtils securityUtils;
    private final PersonService personService;

    @Autowired
    public UserController(SecurityUtils securityUtils, PersonService personService) {
        this.securityUtils = securityUtils;
        this.personService = personService;
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    //@PreAuthorize("isAuthenticated()")
    public List<User> getAllUsers() {
        return personService.findAll();
    }

    @RequestMapping(value = "/current", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<User> getCurrentUser() {
        User user = securityUtils.getCurrentUser();
        return new ResponseEntity<>(user, user != null ? HttpStatus.OK : HttpStatus.NO_CONTENT);
    }

    @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    public List<User> getUserByString(@RequestParam("search") String searchString) {
        if (searchString.length() < 3) {
            return null;
        }
        return personService.findUserByString(searchString);
    }

}
