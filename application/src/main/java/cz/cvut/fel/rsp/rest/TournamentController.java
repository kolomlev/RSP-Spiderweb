package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.dto.FilterDto;
import cz.cvut.fel.rsp.dto.TournamentDTO;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.rest.util.RestUtils;
import cz.cvut.fel.rsp.service.repository.PersonService;
import cz.cvut.fel.rsp.service.repository.TournamentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/tournament")
public class TournamentController extends AbstractController {

    private final TournamentService tournamentService;
    private final PersonService     personService;

    @Autowired
    public TournamentController(TournamentService tournamentService, PersonService personService) {
        this.tournamentService = tournamentService;
        this.personService     = personService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}", produces = MediaType.APPLICATION_JSON_VALUE)
    public Tournament find(@PathVariable Integer id) {
        return tournamentService.find(id);
    }

    @RequestMapping(method = RequestMethod.POST, value = "/filter")
    public List<Tournament> findAllByFilter(@RequestBody FilterDto filterDto) {
        return tournamentService.findTournamentsByFilter(filterDto);
    }


    @RequestMapping(method = RequestMethod.GET)
    public List<Tournament> findAll(){
        return tournamentService.getAll();
    }

    //@ResponseBody
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Tournament> createTournament(@RequestBody Tournament tournament) {
        tournamentService.persist(tournament); // Здесь был create
        if (LOG.isDebugEnabled()) {
            LOG.debug("Tournament {} created.", tournament.getId());
        }
        final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", tournament.getId());
        return new ResponseEntity<>(tournament,headers, HttpStatus.CREATED);
    }

    @RequestMapping(method = RequestMethod.DELETE, value = "/{id}")
    public void deleteTorunament(@PathVariable("id") Integer id){
        tournamentService.deleteTournament(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/participants/{tournament-id}")
    public List<User> findTournamentParticipants(@PathVariable("tournament-id") Integer tournamentId){
        return tournamentService.findTournamentParticipants(tournamentId);
    }

    @RequestMapping(method = RequestMethod.GET, value ="/add-player")
    public void tounamentAddPlayer(@RequestParam(value = "id") Integer tournamentId,
                                   @RequestParam(value = "username") String username) {
        tournamentService.addPlayer(tournamentId, username);
    }

    @RequestMapping(method = RequestMethod.GET, value ="/remove-player")
    public void tounamentRemovePlayer(@RequestParam(value = "id") Integer tournamentId,
                                      @RequestParam(value = "username") String username) {
        tournamentService.removePlayer(tournamentId, username);
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = "/submit")
    public Tournament submitTournament(@RequestBody TournamentDTO dto){
        tournamentService.submitTournament(dto.getId(), dto.getPlayersUsernames());
        return tournamentService.find(dto.getId());
    }

}
