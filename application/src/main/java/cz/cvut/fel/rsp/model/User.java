package cz.cvut.fel.rsp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonManagedReference;
import lombok.Getter;
import lombok.Setter;
import org.mapstruct.Named;
import org.springframework.security.crypto.password.PasswordEncoder;

import javax.persistence.*;
import java.util.Date;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name="person")
@Getter
@Setter
@NamedQueries(@NamedQuery(name = "User.findByString", query = "SELECT u FROM User u WHERE u.username LIKE :username"))
public class User extends AbstractEntity {

    public User() {
    }

    private String username;

    private String password;
    
    private String name;
    
    private String surname;

    @ManyToMany(cascade = CascadeType.ALL)
    @JsonBackReference
//    @JsonIgnore
    @JoinTable(name = "person_tournament", joinColumns =@JoinColumn(name = "person_id"), inverseJoinColumns = @JoinColumn(name = "tournament_id"))
    List<Tournament> tournaments;

    public void encodePassword(PasswordEncoder encoder) {
        Objects.requireNonNull(encoder);
        if (password != null) {
            this.password = encoder.encode(password);
        }
    }
    
    

}
