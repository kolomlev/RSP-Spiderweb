package cz.cvut.fel.rsp.model;

import com.fasterxml.jackson.annotation.*;
import lombok.Getter;
import lombok.Setter;
import org.springframework.core.annotation.Order;

import javax.persistence.*;
import java.util.Date;
import java.util.List;

@Entity
@Getter
@Setter
public class Tournament extends AbstractEntity {

    private String description;

    private String name;

    private String place;

    @Temporal(javax.persistence.TemporalType.DATE)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy", locale = "cs_CZ", timezone = "Europe/Paris")
    private Date date;

    //JsonFormat pridava neustale +1 hodinu a nepodarilo se mi najit rozumnejsi reseni
    //nez ten timezone tak pro ucel tohoto premetu nechame tak
    @Temporal(javax.persistence.TemporalType.TIME)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "hh:mm", timezone = "CET")
    private Date time;

    private String sport;

//    @JsonIgnore
    //@JsonManagedReference
    @OneToMany(mappedBy = "tournament")
    @OrderBy("index ASC")
    private List<Match> matches;

//    @JsonIgnore
    @ManyToOne
    private User organizer;

//    @JsonIgnore
    //@JsonManagedReference
    @ManyToMany(mappedBy = "tournaments")
    private List<User> participants;
}
