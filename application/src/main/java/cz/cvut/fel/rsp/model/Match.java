package cz.cvut.fel.rsp.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class Match extends AbstractEntity {

    @ManyToOne(optional = false)
    @JoinColumn(name="player_one_id")
    private User playerOne;

    @ManyToOne(optional = false)
    @JoinColumn(name="player_two_id")
    private User playerTwo;

    @JsonBackReference
//    @JsonIgnore
    @JoinColumn(name = "tournament")
    @ManyToOne(cascade=CascadeType.PERSIST)
    private Tournament tournament;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="result_one_id")
    private Result resultOne;

    @OneToOne(cascade=CascadeType.ALL)
    @JoinColumn(name="result_two_id")
    private Result resultTwo;

    private String status;

    @JsonIgnore
    private int index;
}
