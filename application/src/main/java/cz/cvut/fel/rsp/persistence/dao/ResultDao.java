package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.model.Result;
import org.springframework.stereotype.Repository;

@Repository
public class ResultDao extends BaseDao<Result>{

    protected ResultDao() {
        super(Result.class);
    }
}
