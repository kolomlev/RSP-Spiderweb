package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.service.repository.MatchService;
import cz.cvut.fel.rsp.service.repository.ResultService;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Map;
import java.util.Objects;
import org.springframework.transaction.annotation.Transactional;

@RestController
@RequestMapping("/match")
public class MatchController extends AbstractController {

    private MatchService matchService;
    private SecurityUtils securityUtils;

    @Autowired
    public MatchController(MatchService matchService, SecurityUtils securityUtils) {
        this.matchService = matchService;
        this.securityUtils = securityUtils;
    }

    @Transactional
    @RequestMapping(method = RequestMethod.POST, value = "/{id}/setResult")
    public Match setResult(@PathVariable("id") int id,
            @RequestBody Map<String, Integer> params) {

        int score1 = params.get("scoreOne");
        int score2 = params.get("scoreTwo");
        matchService.setResult(id, score1, score2);
        return matchService.find(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/getResultOne")
    public Result getResultOne(@PathVariable int id) {
        return matchService.getResultOne(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/getResultTwo")
    public Result getResultTwo(@PathVariable int id) {
        return matchService.getResultTwo(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/getWinner")
    public User getWinner(@PathVariable int id) {
        return matchService.getWinner(id);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/{id}/getLooser")
    public User getLooser(@PathVariable int id) {
        return matchService.getLooser(id);
    }

}
