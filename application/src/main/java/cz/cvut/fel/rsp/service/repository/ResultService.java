package cz.cvut.fel.rsp.service.repository;

import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.dao.GenericDao;
import cz.cvut.fel.rsp.persistence.dao.MatchDao;
import cz.cvut.fel.rsp.persistence.dao.ResultDao;
import cz.cvut.fel.rsp.persistence.dao.UserDao;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Objects;

@Service
public class ResultService extends AbstractRepositoryService<Result> {

    private final ResultDao resultDao;
    private final MatchDao matchDao;
    private final UserDao userDao;
    private final SecurityUtils securityUtils;

    @Autowired
    public ResultService(ResultDao resultDao, MatchDao matchDao, UserDao userDao, SecurityUtils securityUtils) {
        this.resultDao = resultDao;
        this.matchDao = matchDao;
        this.userDao = userDao;
        this.securityUtils = securityUtils;
    }

    @Override
    protected GenericDao<Result> getPrimaryDao() {
        return resultDao;
    }

//
//    public void create(int scoreOne, int scoreTwo, int idMatch){
//
//        Match m = matchDao.find(idMatch);
//        // result not null check
//        Objects.requireNonNull(m);
//
//        // set author to current user
//        User user = securityUtils.getCurrentUser();
//        Objects.requireNonNull(user);
//
//        Result r = new Result();
//        r.setScoreOne(scoreOne);
//        r.setScoreTwo(scoreTwo);
//        r.setMatch(m);
//        r.setAuthor(user);
//        persist(r);
//        matchService.setResult(m,r);
//    }
    public void save(Result result) {
        if (result.getId() == null) {
            persist(result);
        } else {
            update(result);
        }
    }

    public boolean compare(Result r1, Result r2) {
        if (r1 == null || r2 == null) {
            return false;
        }
        return r1.getScoreOne() == r2.getScoreOne() && r1.getScoreTwo() == r2.getScoreTwo();
    }

    public boolean isDraw(Result r1, Result r2) {
        if (r1 == null || r2 == null) {
            return false;
        }
        return r1.getScoreOne() == r1.getScoreTwo() || r2.getScoreOne() == r2.getScoreTwo();
    }

    @Override
    public void prePersist(Result result) {
        Objects.requireNonNull(result.getMatch());
        Objects.requireNonNull(result.getAuthor());
    }
}
