package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.dto.FilterDto;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import org.springframework.stereotype.Repository;

import javax.persistence.criteria.*;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TournamentDao extends BaseDao<Tournament> {
    protected TournamentDao() {
        super(Tournament.class);
    }

    public Tournament findByName(String tournamentName) {
        if (tournamentName == null || tournamentName.isEmpty())
            return null;
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tournament> criteriaQuery = criteriaBuilder.createQuery(Tournament.class);
        Root<Tournament> from = criteriaQuery.from(Tournament.class);
        criteriaQuery.select(from);
        // case-insensitive search
        criteriaQuery.where(criteriaBuilder.equal(criteriaBuilder.upper(from.<String>get("name")),tournamentName));
        List<Tournament> fetchedUsers = em.createQuery(criteriaQuery).getResultList();
        if (fetchedUsers.size() == 0)
            return null;
        else
            return fetchedUsers.get(0);
    }

    public List<Tournament> findTournamentsByFilter(FilterDto filterDto, Integer currentUserId) {
        CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
        CriteriaQuery<Tournament> criteriaQuery = criteriaBuilder.createQuery(Tournament.class);

        Root<Tournament> tournamentRoot = criteriaQuery.from(Tournament.class);

        Subquery<User> participantsSubQuery = criteriaQuery.subquery(User.class);
        Root<Tournament> tournamentSubroot = participantsSubQuery.correlate(tournamentRoot);
        Join<Tournament, User> participants = tournamentSubroot.join("participants");

        List<Predicate> predicates = new ArrayList<>();

        if(filterDto.isOrganizer()){
            predicates.add(criteriaBuilder.equal(tournamentRoot.get("organizer").get("id"), currentUserId));
        }

        if(filterDto.getDate() != null){
            predicates.add(criteriaBuilder.equal(tournamentRoot.get("date"), filterDto.getDate()));
        }

        if(filterDto.isParticipant()){
            participantsSubQuery.select(participants);
            participantsSubQuery.where(criteriaBuilder.equal(participants.get("id"), currentUserId));
            predicates.add(criteriaBuilder.exists(participantsSubQuery));
        }

        criteriaQuery.select(tournamentRoot)
                .where(predicates.toArray(new Predicate[]{}));

        return em.createQuery(criteriaQuery).getResultList();
    }
}
