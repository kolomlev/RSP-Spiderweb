package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.exception.UsernameExistsException;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.rest.util.RestUtils;
import cz.cvut.fel.rsp.service.repository.PersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/registration")
public class RegistrationController extends AbstractController {

    private final PersonService personService;

    @Autowired
    public RegistrationController(PersonService personService) {
        this.personService = personService;
    }

    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> registerUser(@RequestBody User info) {

        User user = new User();
        user.setUsername(info.getUsername());
        user.setPassword(info.getPassword());
        user.setName(info.getName());
        user.setSurname(info.getSurname());

        try {
            personService.persist(user);
            if (LOG.isDebugEnabled()) {
                LOG.debug("User {} created.", user.getId());
            }
            final HttpHeaders headers = RestUtils.createLocationHeaderFromCurrentUri("/{id}", user.getId());
            return new ResponseEntity<Void>(headers, HttpStatus.CREATED);
        } catch (UsernameExistsException e) {
            return new ResponseEntity<Void>(HttpStatus.CONFLICT);
        }

    }
}
