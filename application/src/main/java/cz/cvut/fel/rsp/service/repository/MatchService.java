package cz.cvut.fel.rsp.service.repository;

import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.dao.*;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import javafx.scene.input.TouchEvent;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.logging.Logger;

@Service
public class MatchService extends AbstractRepositoryService<Match> {

    private final MatchDao matchDao;
    private final UserDao userDao;
    private final TournamentDao tournamentDao;
    private final ResultDao resultDao;
    private final SecurityUtils securityUtils;
    private final ResultService resultService;

    @Autowired
    public MatchService(MatchDao matchDao, UserDao userDao, ResultDao resultDao,
            SecurityUtils securityUtils, ResultService resultService, TournamentDao tournamentDao) {
        this.matchDao = matchDao;
        this.userDao = userDao;
        this.resultDao = resultDao;
        this.securityUtils = securityUtils;
        this.resultService = resultService;
        this.tournamentDao = tournamentDao;
    }

//    public void create(int tournamentId, int idPlayerOne, int idPlayerTwo) {
//        Tournament tournament = tournamentDao.find(tournamentId);
//        Objects.requireNonNull(tournament);
//        User p1 = userDao.find(idPlayerOne);
//        User p2 = userDao.find(idPlayerTwo);
//        create(tournament, p1, p2);
//    }
//
//    public void create(String tournamentName, String usrnamePlayerOne, String usrNamePlayerTwo) {
//        Tournament tournament = tournamentDao.findByName(tournamentName);
//        User p1 = userDao.findByUsername(usrnamePlayerOne);
//        User p2 = userDao.findByUsername(usrNamePlayerTwo);
//
//        create(tournament, p1, p2);
//    }
//
//    /**
//     * Creates match with given paramatres. Initially, match state is set to
//     * 'TBD'. It can be later changed explicitly with setResult method,
//     * Which sets both result and state
//     *
//     * @param tournament
//     * @param playerOne
//     * @param playerTwo
//     */
//    public Match create(Tournament tournament, User playerOne, User playerTwo) {
//        // tournament not-null check
//        Objects.requireNonNull(tournament);
//        // players not-null check
//        if (!(playerOne == null && playerTwo == null)) {
//            if (playerOne.equals(playerTwo)) {
//                return null;
//            }
//        }
//        // two players are same one user
//
//        Match m = new Match();
//        m.setTournament(tournament);
//        m.setPlayerOne(playerOne);
//        m.setPlayerTwo(playerTwo);
//        m.setStatus("TBD");
//        tournament.getMatches().add(m);
//        persist(m);
//        return m;
//    }
//
//    public void create(Tournament tournament, User playerOne) {
//        create(tournament, playerOne, null);
//    }
//
//    public void create(Tournament tournament) {
//        create(tournament, null, null);
//    }
    public void setPlayer(Match match, User user, boolean isPlayerOne) {
        Objects.requireNonNull(match);
        Objects.requireNonNull(user);

        if (isPlayerOne) {
            match.setPlayerOne(user);
        } else {
            match.setPlayerTwo(user);
        }
        update(match);
    }

    /**
     *
     * Sets the result to the match and controls match status by comparing
     * results.
     *
     * If result's author is tournament organizer, sets both results If result's
     * author is first player, sets first result, if second - sets second. If
     * result's author is any other user, does nothing.
     *
     *
     * @param id
     * @param score1
     * @param score2
     */
    public void setResult(int id, int score1, int score2) {
        //get match
        Match match = find(id);

        //Check for null values
        Objects.requireNonNull(match);
        Objects.requireNonNull(match.getPlayerOne());
        Objects.requireNonNull(match.getPlayerTwo());

        Result result = new Result();
        //Check if current user can change match result(User is player or organizer)
        if (hasPrivilageToEditResult(match)) {
            User currentUser = securityUtils.getCurrentUser();
            //if user is organizer set both result
            if (currentUser.equals(match.getTournament().getOrganizer())) {
                match.setResultOne(result);
                match.setResultTwo(result);

                //if user is player1 set result1
            } else if (currentUser.equals(match.getPlayerOne())) {
                result = match.getResultOne() != null ? match.getResultOne() : new Result();
                match.setResultOne(result);
                //if user is player2 set result2
            } else if (currentUser.equals(match.getPlayerTwo())) {
                result = match.getResultTwo() != null ? match.getResultTwo() : new Result();
                match.setResultTwo(result);
            }
            //set common data
            result.setMatch(match);
            result.setAuthor(currentUser);
            result.setScoreOne(score1);
            result.setScoreTwo(score2);

            //persist or update depending on if the match already had results
            resultService.save(result);
            this.setMatchStatus(match);

            if (match.getStatus().equals("OK")) {
                Tournament t = match.getTournament();
                List<Match> matches = t.getMatches();
                int position = matches.indexOf(match);
                Match next = matches.get((position - 1) / 2);
                int mod = (position - 1) % 2;
                setPlayer(next, getWinner(match.getId()), mod == 0);
                update(next);
            }

            //update match
            update(match);
        }
    }


    /*If both players hasnt submitted result yet - TBD
      If both submitted and results are different - Conflict
      If both submitted and results are the same - OK
     */
    private void setMatchStatus(Match match) {
        if (match.getResultOne() == null || match.getResultTwo() == null) {
            match.setStatus("TBD");
        } else if (!resultService.compare(match.getResultOne(), match.getResultTwo())
                || resultService.isDraw(match.getResultOne(), match.getResultTwo())) {
            match.setStatus("CONFLICT");
        } else {
            match.setStatus("OK");
        }
    }

    private boolean hasPrivilageToEditResult(Match match) {
        return securityUtils.getCurrentUser().equals(match.getPlayerOne())
                || securityUtils.getCurrentUser().equals(match.getPlayerTwo())
                || securityUtils.getCurrentUser().equals(match.getTournament().getOrganizer());
    }

    public void resetResult(int idMatch, int idResult) {
        resetResult(find(idMatch), resultDao.find(idResult));
    }

    /**
     * Resets the result of the match. Checks status at the end to be consisted
     *
     * @param match
     * @param result
     */
    public void resetResult(Match match, Result result) {
        // not null check
        Objects.requireNonNull(match);
        Objects.requireNonNull(result);

        // check if match and result are persisted
        if (!(matchDao.exists(match.getId()) && resultDao.exists(result.getId()))) {
            return;
        }

        if (match.getResultOne().equals(result)) {
            match.setResultOne(null);
        } else if (match.getResultTwo().equals(result)) {
            match.setResultTwo(null);
        }
        setMatchStatus(match);
    }

    /**
     * Resets both results of the match
     *
     * @param match
     */
    public void resetResult(Match match) {
        resetResult(match, match.getResultOne());
        resetResult(match, match.getResultTwo());
    }

    public User getWinner(int id) {
        Match m = find(id);

        if (m.getStatus().equals("OK")) {

            if (m.getPlayerOne() == null && m.getPlayerTwo() == null) {
                return null;
            }

            if (m.getPlayerOne() == null && m.getPlayerTwo() != null) {
                return m.getPlayerTwo();
            } else if (m.getPlayerOne() != null && m.getPlayerTwo() == null) {
                return m.getPlayerOne();
            } else if (m.getResultOne().getScoreOne() > m.getResultOne().getScoreTwo()) {
                return m.getPlayerOne();
            } else {
                return m.getPlayerTwo();
            }

        } else {
            return null;
        }

    }

    public User getLooser(int id) {
        Match m = find(id);
        Objects.requireNonNull(m);

        if (m.getStatus().equals("OK")) {
            if (m.getPlayerOne() == null || m.getPlayerTwo() == null) {
                return null;
            }

            if (m.getResultOne().getScoreOne() < m.getResultOne().getScoreTwo()) {
                return m.getPlayerOne();
            } else {
                return m.getPlayerTwo();
            }
        } else {
            return null;
        }
    }

    public Result getResultOne(int id) {
        Match m = find(id);
        Objects.requireNonNull(m);
        return m.getResultOne();
    }

    public Result getResultTwo(int id) {
        Match m = find(id);
        Objects.requireNonNull(m);
        return m.getResultTwo();
    }

    @Override
    public void prePersist(Match match) {
        Objects.requireNonNull(match.getTournament());
    }

    @Override
    protected GenericDao<Match> getPrimaryDao() {
        return matchDao;
    }
}
