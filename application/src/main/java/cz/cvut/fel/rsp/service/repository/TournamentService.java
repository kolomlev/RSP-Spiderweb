package cz.cvut.fel.rsp.service.repository;

import cz.cvut.fel.rsp.dto.FilterDto;
import cz.cvut.fel.rsp.model.Match;
import cz.cvut.fel.rsp.model.Tournament;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.dao.GenericDao;
import cz.cvut.fel.rsp.persistence.dao.TournamentDao;
import cz.cvut.fel.rsp.persistence.dao.UserDao;
import cz.cvut.fel.rsp.service.security.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Date;
import java.sql.Time;
import java.util.*;

@Service
public class TournamentService extends AbstractRepositoryService<Tournament> {

    private final TournamentDao tournamentDao;
    private final UserDao       userDao;
    private final MatchService  matchService;
    private final SecurityUtils securityUtils;

    @Autowired
    public TournamentService(TournamentDao tournamentDao, UserDao userDao, SecurityUtils securityUtils, MatchService matchService) {
        this.tournamentDao = tournamentDao;
        this.userDao = userDao;
        this.securityUtils = securityUtils;
        this.matchService  = matchService;
    }

    @Override
    protected GenericDao<Tournament> getPrimaryDao() {
        return tournamentDao;
    }

    public List<Tournament> getAll() {
        List<Tournament> tournaments = findAll();
        return tournaments;
    }

    public boolean addPlayer(int t, String username){

        // username is invalid
        if (username == null || username.isEmpty()) {
            return false;
        }

        User user = userDao.findByUsername(username);
        // user is persisted assert
        Objects.requireNonNull(user);

        Tournament tournament = find(t);
        // tournament is persisted assert
        Objects.requireNonNull(tournament);

        // user is already in tournament
        if (tournament.getParticipants().contains(user)) {
            return false;
        }

        tournament.getParticipants().add(user);
        user.getTournaments().add(tournament);
        userDao.update(user);
        update(tournament);
        return true;
    }

    public boolean removePlayer(int t, String username){

        // username is invalid
        if (username == null || username.isEmpty()) {
            return false;
        }

        User user = userDao.findByUsername(username);
        // user is persisted assert
        Objects.requireNonNull(user);

        Tournament tournament = find(t);
        // tournament is persisted assert
        Objects.requireNonNull(tournament);

        // user is already not in tournament
        if (!tournament.getParticipants().contains(user)) {
            return false;
        }

        tournament.getParticipants().remove(user);
        user.getTournaments().remove(tournament);
        userDao.update(user);
        update(tournament);
        return true;
    }


    public List<User> getAllPlayers(int t) {
        Tournament tournament = find(t);

        // tournament is not persisted
        Objects.requireNonNull(tournament);
        return tournament.getParticipants();
    }

    public boolean addMatch(int id, Match match) {
        Objects.requireNonNull(match);
        Tournament tournament = find(id);
        Objects.requireNonNull(tournament);
        if(tournament.getMatches().contains(match)) {
           return false;
        }
        tournament.getMatches().add(match);
        return true;
    }

    public void submitTournament(int id, List<String> usernames ) {
        usernames.forEach(name -> addPlayer(id, name));
        createMatches(id);
    }

    private void createMatches(int id) {
        Tournament t = find(id);
        List<Match> matches = new ArrayList<>();
        int order = 0;
        for (int i = 0; i < (nextPowerOfTwo(t.getParticipants().size()) - 1) / 2; i++) {
            Match m = new Match();
            m.setTournament(t);
            m.setIndex(order++);
            matches.add(m);
        }
        int index = nextPowerOfTwo(t.getParticipants().size()) - t.getParticipants().size();
        for (int i = index; i < t.getParticipants().size() - 1; i += 2) {
            Match m = new Match();
            m.setTournament(t);
            m.setPlayerOne(t.getParticipants().get(i));
            m.setPlayerTwo(t.getParticipants().get(i+1));
            m.setIndex(order++);
            matches.add(m);

        }
        for (int i = 0; i < index; i++) {
            Match m = new Match();
            m.setTournament(t);
            m.setPlayerOne(t.getParticipants().get(i));
            m.setPlayerTwo(null);
            m.setStatus("OK");
            m.setIndex(order++);
            matches.add(m);
        }
        t.setMatches(matches);
        matchService.persist(matches);

        //Free win condition
        for (int i = 0; i < index; i++) {
            Match match  = t.getMatches().get(t.getMatches().size() - (i + 1) );
            int position = t.getMatches().indexOf(match);
            Match next   = t.getMatches().get( (position - 1 ) / 2);
            int mod = (position-1)%2;
            matchService.setPlayer(next,match.getPlayerOne(), mod ==0);
            matchService.update(next);
        }

        update(t);
    }

    private boolean isPowerOfTwo(int n) {
        while (n > 1) {
            if (n % 2 != 0) {
                return false;
            }
            n /= 2;
        }
        return true;
    }

    private int nextPowerOfTwo(int n) {
        int res = n;
        while (!isPowerOfTwo(res)){
            res++;
        }
        return res;
    }


    public List<Tournament> findTournamentsByFilter(FilterDto filterDto){
        return tournamentDao.findTournamentsByFilter(filterDto,  securityUtils.getCurrentUser().getId());
    }


    @Override
    public void prePersist(Tournament tournament){
        Objects.requireNonNull(tournament);
        Objects.requireNonNull(tournament.getName());

        //Uncomment when getCurrentUser() will be avaliable
        tournament.setOrganizer(securityUtils.getCurrentUser());

        // initializing lists
        tournament.setParticipants(new ArrayList<>());
        tournament.setMatches(new ArrayList<>());
    }

    public List<User> findTournamentParticipants(Integer tournamentId) {
        Tournament tournament = find(tournamentId);

        if(tournament == null){
            return new ArrayList<User>();
        }

        if(tournament.getParticipants() == null){
            return new ArrayList<User>();
        }

        return tournament.getParticipants();

    }

    @Transactional
    public void deleteTournament(Integer id) {
        remove(find(id));
    }
}
