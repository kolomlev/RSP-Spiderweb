package cz.cvut.fel.rsp.rest;

import cz.cvut.fel.rsp.model.Result;
import cz.cvut.fel.rsp.service.repository.ResultService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Objects;

@RestController
@RequestMapping("/result")
public class ResultController extends AbstractController {

    private final ResultService resultService;

    @Autowired
    public ResultController(ResultService resultService) {
        this.resultService = resultService;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/{id}/addNotes")
    public void editNotes(@PathVariable("id") int id, @RequestBody String notes){
        Result r = resultService.find(id);
        Objects.requireNonNull(r);

        r.setNotes(notes);
        resultService.update(r);
    }


    @RequestMapping(method = RequestMethod.GET, value = "/{id}/")
    public Result get(@PathVariable("id") int id){
        return resultService.find(id);
    }


}
