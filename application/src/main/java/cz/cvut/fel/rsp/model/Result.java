package cz.cvut.fel.rsp.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;

@Entity
@Getter
@Setter
public class Result extends AbstractEntity {

    private int scoreOne;

    private int scoreTwo;
    
    @JsonIgnore
    @OneToOne
    @JoinColumn(name = "match_id")
    private Match match;
    
    @OneToOne
    @JoinColumn(name = "author_id")
    private User author;

    private String notes;

}
