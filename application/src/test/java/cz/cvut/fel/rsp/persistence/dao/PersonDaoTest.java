package cz.cvut.fel.rsp.persistence.dao;

import cz.cvut.fel.rsp.environment.Generator;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.persistence.BaseDaoTestRunner;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

import static org.junit.Assert.*;

public class PersonDaoTest extends BaseDaoTestRunner {

    @Autowired
    private UserDao dao;

    @Test
    public void findByUsernameReturnsPersonWithMatchingUsername() {
        final User person = Generator.generatePerson();
        dao.persist(person);

        final User result = dao.findByUsername(person.getUsername());
        assertNotNull(result);
        assertEquals(person.getId(), result.getId());
    }

    @Test
    public void findByUsernameReturnsNullForUnknownUsername() {
        assertNull(dao.findByUsername("unknownUsername"));
    }

    @Test
    public void existsReturnsTrueForExistingUsername() {
        final User person = Generator.generatePerson();
        dao.persist(person);
        assertTrue(dao.exists(person.getUsername()));
    }

    @Test
    public void existsReturnsFalseForUnknownUsername() {
        assertFalse(dao.exists("someArbitraryUsername"));
    }
}