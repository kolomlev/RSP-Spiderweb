package cz.cvut.fel.rsp.service.security;

import cz.cvut.fel.rsp.environment.Environment;
import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.security.model.UserDetails;
import cz.cvut.fel.rsp.service.BaseServiceTestRunner;
import cz.cvut.fel.rsp.environment.Generator;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import static org.hamcrest.CoreMatchers.containsString;
import static org.junit.Assert.*;

public class SecurityUtilsTest extends BaseServiceTestRunner {

    @Rule
    public ExpectedException thrown = ExpectedException.none();

    @Autowired
    private SecurityUtils securityUtils;

    private User person;

    @Before
    public void setUp() {
        this.person = Generator.generatePerson();
        person.setId(Generator.randomInt());
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
    }

    @Test
    public void getCurrentUserReturnsCurrentlyLoggedInUser() {
        Environment.setCurrentPerson(person);
        final User result = securityUtils.getCurrentUser();
        assertEquals(person, result);
    }

    @Test
    public void getCurrentUserDetailsReturnsUserDetailsOfCurrentlyLoggedInUser() {
        Environment.setCurrentPerson(person);
        final UserDetails result = securityUtils.getCurrentUserDetails();
        assertNotNull(result);
        assertTrue(result.isEnabled());
        assertEquals(person, result.getUser());
    }

    @Test
    public void getCurrentUserDetailsReturnsNullIfNoUserIsLoggedIn() {
        assertNull(securityUtils.getCurrentUserDetails());
    }

    @Test
    public void updateCurrentUserReplacesUserInCurrentSecurityContext() {
        Environment.setCurrentPerson(person);
        final User update = new User();
        update.setPassword(person.getPassword());
        update.setUsername(person.getUsername());
        userDao.update(update);
        securityUtils.updateCurrentUser();

        final User currentPerson = securityUtils.getCurrentUser();
        assertEquals(update, currentPerson);
    }

    @Test
    public void verifyCurrentUserPasswordThrowsIllegalArgumentWhenPasswordDoesNotMatch() {
        Environment.setCurrentPerson(person);
        final String password = "differentPassword";
        thrown.expect(IllegalArgumentException.class);
        thrown.expectMessage(containsString("does not match"));
        securityUtils.verifyCurrentUserPassword(password);
    }
}