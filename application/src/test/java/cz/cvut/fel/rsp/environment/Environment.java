package cz.cvut.fel.rsp.environment;

import cz.cvut.fel.rsp.model.User;
import cz.cvut.fel.rsp.security.model.UserDetails;
import cz.cvut.fel.rsp.security.model.AuthenticationToken;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.context.SecurityContextImpl;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.security.Principal;
import java.util.HashSet;

public class Environment {

    private static User currentPerson;

    private Environment() {
        throw new AssertionError();
    }

    /**
     * Initializes security context with the specified person.
     *
     * @param person Person to set as currently authenticated
     */
    public static void setCurrentPerson(User person) {
        currentPerson = person;
        final UserDetails userDetails = new UserDetails(person, new HashSet<>());
        SecurityContext context = new SecurityContextImpl();
        context.setAuthentication(new AuthenticationToken(userDetails.getAuthorities(), userDetails));
        SecurityContextHolder.setContext(context);
    }

    /**
     * Gets current user as security principal.
     *
     * @return Current user authentication as principal or {@code null} if there is no current user
     */
    public static Principal getCurrentUserPrincipal() {
        if (SecurityContextHolder.getContext() == null) {
            throw new IllegalStateException("SecurityContext is empty!");
        }
        return SecurityContextHolder.getContext().getAuthentication();
    }

    public static User getCurrentPerson() {
        return currentPerson;
    }

    /**
     * Loads data from the specified file.
     *
     * @param fileName Name of the file to load data from (including path). It is expected to be on the classpath and
     *                 Classloader is used to get input stream for the file.
     * @return Content of the file
     * @throws Exception If file reading fails
     */
    public static String loadData(String fileName) throws Exception {
        try (final BufferedReader in = new BufferedReader(
                new InputStreamReader(Environment.class.getClassLoader().getResourceAsStream(fileName)))) {
            final StringBuilder builder = new StringBuilder();
            String line;
            while ((line = in.readLine()) != null) {
                builder.append(line).append('\n');
            }
            return builder.toString();
        }
    }
}
