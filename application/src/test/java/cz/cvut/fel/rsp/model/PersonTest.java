package cz.cvut.fel.rsp.model;

import cz.cvut.fel.rsp.environment.Generator;
import org.junit.Test;
import org.springframework.security.crypto.password.PasswordEncoder;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.*;

public class PersonTest {

    @Test
    public void encodePasswordEncodesInstancePassword() {
        final User p = Generator.generatePerson();
        final String originalPassword = p.getPassword();
        final String encodedPassword = "encodedPassword";
        final PasswordEncoder encoder = mock(PasswordEncoder.class);
        when(encoder.encode(p.getPassword())).thenReturn(encodedPassword);
        p.encodePassword(encoder);
        verify(encoder).encode(originalPassword);
        assertEquals(encodedPassword, p.getPassword());
    }
}