package cz.cvut.fel.rsp.environment;

import cz.cvut.fel.rsp.model.User;

import java.net.URI;
import java.util.Random;

public class Generator {

    private static final Random RANDOM = new Random();
    private static final String TYPE_BASE = "http://onto.fel.cvut.cz/ontologies/aso/event-type";

    private Generator() {
        throw new AssertionError();
    }

    public static User generatePerson() {
        final User p = new User();
        p.setUsername("halsey" + RANDOM.nextInt(1000) + "@unsc.org");
        p.setPassword("1Cr3@t3dSp@rt@ns");
        return p;
    }

    public static int randomInt() {
        return RANDOM.nextInt();
    }

    public static int randomInt(int max) {
        return RANDOM.nextInt(max);
    }

    public static int randomInt(int min, int max) {
        assert min < max;
        int result;
        do {
            result = randomInt(max);
        } while (result < min);
        return result;
    }

    public static URI generateTypeUri() {
        return URI.create(TYPE_BASE + randomInt());
    }




}
