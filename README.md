### Navigate to application
This is my path: cd /c/users/denky/desktop/cvut/semestr4/rsp/rsp-spiderweb/application

###Build image for application
docker build -f Dockerfile -t spiderweb .

###Running postgres container
docker run --name sw-postgres -e POSTGRES_PASSWORD=rsp -e POSTGRES_DB=rspdva -e POSTGRES_USER=rsp -d postgres

###Running spiderweb container and link it to postgres container
docker run -p 8080:8080 --name spiderweb-psql --link sw-postgres:postgres -d spiderweb

###Run spiderweb application
docker logs spiderweb-psql

###Open in browser
http://192.168.99.100:8080/Pavouk